package com.cars24.automation.APITesting;

import static com.cars24.utilities.YamlReader.getData;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.cars24.utilities.YamlReader;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class DoTransactionAPITest {

	WebResource webResource;
	
	@BeforeClass
	public void setup(){
		YamlReader.setYamlFilePath();
	}
	
	@Test(dataProvider = "dataProvider")
	public void Test1(String transactionType1, String statusCode1, String transactionType2, String statusCode2, String transactionType3, String statusCode3, String transactionType4, String statusCode4, String dealerId, String bidId, String appointmentId){
		String baseuri = getData("apiURL");
		ClientConfig clientConfig = new DefaultClientConfig();              
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);     
		Client client = Client.create(clientConfig);

		webResource = client.resource(baseuri);	
        Assert.assertEquals(getStatus(transactionType1, dealerId, bidId, appointmentId), statusCode1);
        Reporter.log("Status is "+statusCode1+" for "+transactionType1, true);
        if(statusCode2.equals("422")){
        	Assert.assertEquals(getStatus(transactionType2, bidId, appointmentId), statusCode2);
        }
        else{
        	Assert.assertEquals(getStatus(transactionType2, dealerId, bidId, appointmentId), statusCode2);	
        }
        Reporter.log("Status is "+statusCode2+" for "+transactionType2, true);
        if(statusCode3.equals("422")){
        	Assert.assertEquals(getStatus(transactionType3, bidId, appointmentId), statusCode3);
        }
        else{
        	Assert.assertEquals(getStatus(transactionType3, dealerId, bidId, appointmentId), statusCode3);	
        }
        Reporter.log("Status is "+statusCode3+" for "+transactionType3, true);
        Assert.assertEquals(getStatus(transactionType4, dealerId, bidId, appointmentId), statusCode4);
        Reporter.log("Status is "+statusCode4+" for "+transactionType4, true);
		}
	
	public String getStatus(String transactionType, String dealerId, String bidId, String appointmentId){
		Map<String,String> postBody = new HashMap<String,String>();
		postBody.put("dealerId", dealerId);
		postBody.put("transactionType", transactionType);
		postBody.put("bidId", bidId);
		postBody.put("appointmentId", appointmentId);
		ClientResponse response = webResource.accept("application/json")
		                .type("application/json").post(ClientResponse.class, postBody);
		return response.getStatus()+"";
	}
	
	public String getStatus(String transactionType, String bidId, String appointmentId){
		Map<String,String> postBody = new HashMap<String,String>();
		postBody.put("transactionType", transactionType);
		postBody.put("bidId", bidId);
		postBody.put("appointmentId", appointmentId);
		ClientResponse response = webResource.accept("application/json")
		                .type("application/json").post(ClientResponse.class, postBody);
		return response.getStatus()+"";
	}
	
	@DataProvider
	public Object[][] dataProvider(){
		Object[][] data = new Object[1][11];
		int noOfTestData = Integer.parseInt(getData("noOfTestData"));
		for(int i=0;i<noOfTestData;i++){
				data[i][0] = getData("data"+(i+1)+".transactionType1");
				data[i][1] = getData("data"+(i+1)+".status1");
				data[i][2] = getData("data"+(i+1)+".transactionType2");
				data[i][3] = getData("data"+(i+1)+".status2");
				data[i][4] = getData("data"+(i+1)+".transactionType3");
				data[i][5] = getData("data"+(i+1)+".status3");
				data[i][6] = getData("data"+(i+1)+".transactionType4");
				data[i][7] = getData("data"+(i+1)+".status4");
				data[i][8] = getData("data"+(i+1)+".dealerId");
				data[i][9] = getData("data"+(i+1)+".bidId");
				data[i][10] = getData("data"+(i+1)+".appointmentId");
		}		
		return data;
	}

	}
