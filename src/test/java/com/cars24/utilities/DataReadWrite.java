package com.cars24.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.Properties;

public class DataReadWrite {

	/**
     * writeDataToFile
     *
     * @param Property
     * @param Data
     * @return true if written else return false
     */
    public static boolean writeDataToFile(String Property, String Data) {
        PrintWriter pw = null;
        boolean result = false;
        try {
            String path = "./src/test/resources/testdata";
            String dataFolderPath = URLDecoder.decode(path, "UTF-8");
            String outFilePath = dataFolderPath + File.separator + "appointments.tmp";
            pw = new PrintWriter(new BufferedWriter(new FileWriter(outFilePath, true)));
            pw.println(Property + "=" + Data);

            result = true;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (pw != null) {
                pw.close();
            }

        }

        return result;
    }

    /**
     * readDataFromFile
     *
     * @param Property
     * @return text
     */
    public static String readDataFromFile(String Property) {
        try {
            Properties prop = ResourceLoader.loadProperties("./src/test/resources/testdata/appointments.tmp");
            return prop.getProperty(Property);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
	
}
